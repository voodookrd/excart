<?php

function excart_order_checkout_page($has_block = FALSE) {
  $cart = excart_get_cart();
  if (empty($cart['items'])) {
    if ($has_block) {
      return array();
    }
    else {
      drupal_access_denied();
      return array();
    }
  }
  $items = $cart['items'];
  foreach ($items as $item) {
    $rows[] = array(
      'data' => array(
        array('data' => $item['title'], 'class' => array('title')),
        $item['qty'],
        $item['price'],
        $item['qty'] * $item['price']
      ),
    );
  }

  $table = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => array(
      t('Product name'),
      t('Qty'),
      t('Price') . ', ' . t('RUR'),
      t('Total') . ', ' . t('RUR')
    )
  );
  $out['table'] = $table;
  $out['table']['#prefix'] = '<div class="table-label">' . t('Shopping list') . '</div>';
  $out['table']['#suffix'] = '<div class="table-total">' . t('Total: !sum rub', array('!sum' => $cart['total'])) . '</div>';
  $out['form'] = drupal_get_form('excart_order_form');
  $out['#prefix'] = '<div id="excart-order">';
  $out['#suffix'] = '</div>';
  drupal_alter('excart_order_checkout_data', $out);

  return array($out);
}

function excart_order_checkout_complete() {
  $cart = excart_get_cart();
  $order_id = $cart['order_id'];
  $message = excart_order_get_message($order_id);
  excart_cart_clear_cart();
  return $message;
}

function excart_order_check_page() {
  $query = db_select('excart_orders', 'o')
      ->condition('o.status', 0)
      ->fields('o', array('oid'));
  $count = $query->countQuery()->execute()->fetchField();
  if (isset($_POST['ajax'])) {
    drupal_json_output(array('count' => $count));
    exit;
  }
  return $count;
}
