<?php

function excart_order_admin_orders() {
  return drupal_get_form('excart_order_settings_form');
}

function excart_order_settings_form($form, $form_state) {
  $form['excart_order_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Manager Email address'),
    '#default_value' => variable_get('excart_order_email', '')
  );
  $form['excart_order_message'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('excart_order_message', ''),
    '#title' => t('Order complate message'),
    '#rows' => 4,
  );
  $form['excart_order_cmessage'] = array(
    '#type' => 'textarea',
    '#title' => t('Client meessage on order complete'),
    '#default_value' => variable_get('excart_order_cmessage', ''),
    '#rows' => 6
  );
  $form['excart_order_amessage'] = array(
    '#type' => 'textarea',
    '#title' => t('Admin meessage on order complete'),
    '#default_value' => variable_get('excart_order_amessage', ''),
    '#rows' => 6
  );
  $form['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['tokens']['info'] = array(
    '#type' => 'item',
    '#title' => t('Tokens'),
    '#markup' => t('You can use tokens <br>
            !order_id - Order id <br>
            !order_link - Link to order<br>
            !site_name - Site name <br>
            !site_url - Site url<br>
            !order_items - Order<br>
            !order_info - Delivery info<br>
            !delivery_type - Delivery Type<br>
            !order_total
            ')
  );
  return system_settings_form($form);
}

function excart_order_page($archive = FALSE) {
  if ($archive) {
    $status = 1;
  }
  else {
    $status = 0;
  }
  $query = db_select('excart_orders', 'o')
          ->fields('o')
          ->condition('o.status', $status)
          ->orderBy('o.oid', 'DESC')
          ->execute()->fetchAll();

  $out = array();
  $rows = array();
  $table_row = array();
  foreach ($query as &$row) {
    $items = array();
    $row->order_data = unserialize($row->order_data);
    $row->shipping_data = unserialize($row->shipping_data);
    $cart['items'] = $row->order_data;
    $items[] = l(t('Details'), 'admin/orders/view/' . $row->oid);
    if (!$archive)
      $items[] = l(t('Order complate'), 'admin/orders/complate/' . $row->oid);

    $links = theme('item_list', array(
      'items' => $items,
      'type' => 'ul'
    ));
    $row_table[] = array(
      $row->oid,
      format_date($row->created, 'custom', 'd.m.Y - h:i'),
      $row->shipping_data['data']['name'],
      $row->shipping_data['data']['tel'],
      excart_calculate_cart_total($cart, FALSE) . ' руб.',
      $links
    );
  }
  $headers = array(
    '№',
    t('Post date'),
    t('Name'),
    t('Tel'),
    t('Order total price'),
    t('Actions')
  );
  return array(
    '#theme' => 'table',
    '#rows' => $row_table,
    '#header' => $headers
  );
}

function excart_single_order_page($oid) {
  $query = db_select('excart_orders', 'o')
          ->fields('o')
          ->condition('o.oid', $oid)
          ->orderBy('o.oid', 'DESC')
          ->execute()->fetchAll();
  $out = array();

  if ($query) {
    $orders = l(t('All orders'), 'admin/orders');
    $order = array_shift($query);
    $order->order_data = unserialize($order->order_data);
    $order->shipping_data = unserialize($order->shipping_data);
    $info = excart_order_creat_table($order->shipping_data['data']);
    $products = excart_order_creat_table($order->order_data, 'data');
    $cart['items'] = $order->order_data;
    $total = excart_calculate_cart_total($cart, FALSE);
    $total = 'Итого: ' . $total . ' руб.';
    drupal_add_css(drupal_get_path('module', 'excart_order') . '/exo.css');
    return $orders . $products . '<div class="total">' . $total . '</div><br>' . $info;
  }

  return '';
}

function excart_order_complate_page($oid) {
  $query = db_update('excart_orders')
      ->fields(array(
        'status' => 1
      ))
      ->condition('oid', $oid)
      ->execute();
  drupal_set_message(t('Order #!num closed.', array('!num' => $oid)));
  drupal_goto('admin/orders');
}

function excart_order_archive_page() {
  $table['link'] = array('#markup' => l(t('All orders'), 'admin/orders'));
  $table['table'] = excart_order_page(TRUE);
  return $table;
}

function excart_order_admin_chekcout() {
  $form['excart_checkout_page_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Checkuot page exists'),
    '#description' => t('If unchek this checkbox, you can disable checkout page'),
    '#default_value' => variable_get('excart_checkout_page_access', 1)
  );
  $form['excart_checkout_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create checkuot block'),
    '#description' => t('You can create checkout block'),
    '#default_value' => variable_get('excart_checkout_block', 0)
  );
  $form = system_settings_form($form);
  $form['#submit'][] = 'excart_order_admin_checkout_submit';
  return $form;
}

function excart_order_admin_checkout_submit($form, $form_state) {
  cache_clear_all('*', 'cache_menu', TRUE);
  cache_clear_all('*', 'cache_block', TRUE);
  menu_rebuild();
}
