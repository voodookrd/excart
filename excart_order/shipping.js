(function($){
    Drupal.behaviors.excart = {
        attach: function (context, settings) {  
            var $id = '#'+Drupal.settings.excart_order_id;
            var $element = $($id);
            $element.tabs();            
        }
    }
})(jQuery)