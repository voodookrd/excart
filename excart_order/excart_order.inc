<?php
function excart_order_save(&$order){
    if(isset($order['oid'])){
        drupal_write_record('excart_orders',$order,'oid');
    }
    else{
        drupal_write_record('excart_orders',$order);
    }    
}

function excart_order_load($id){
    $query = db_select('excart_orders','o')
            ->condition('o.oid',$id)
            ->fields('o')
            ->execute()->fetchObject();
    return $query;
}