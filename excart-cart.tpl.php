<div id="cart-page">
  <table class="cart-table">
    <thead>
      <tr>
        <th class="cart-title">Наименование товара</th>
        <th class="cart-qty">Кол.-во</th>
        <th class="cart-price">Цена</th>
        <th class="cart-total">Итог</th>
        <th class="cart-action">Действия</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $nid => $product): ?>
        <tr<?php print $product['options']['row_attributes']; ?>>
          <td class="cart-title"><?php print $product['title']; ?></td>
          <td class="cart-qty">
            <div class="qty-wrap">
              <?php print $product['links']['remove']; ?>
              <?php print $product['qty_html']; ?>
              <?php print $product['links']['add']; ?>
            </div>
          </td>
          <td class="cart-price"><?php print $product['price_html']; ?></td>
          <td class="cart-total"><?php print $product['total_price']; ?></td>
          <td class="cart-actions"><?php print $product['links']['delete']; ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <div class="total-price"><span>Итого:</span><span id="total-price"><?php print $total; ?></span></div>
</div>