<?php

class excart_views_handler_field_addtocart extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['type'] = array(
      'table' => 'node',
      'field' => 'type',
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    if (excart_is_product($values->node_type)) {
      $nid = $values->nid;
      return drupal_get_form('excart_add_to_cart_form_' . $nid, $nid, $this->options['qty_field']);
    }
    return '';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['qty_field'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['qty_field'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable qty field'),
      '#default_value' => $this->options['option_a'],
      '#description' => t('Enabel qty field on add to cart form'),
      '#weight' => -10,
    );
  }

}
