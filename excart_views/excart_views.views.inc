<?php

/**
 * Implements hook_views_data()
 */
function excart_views_views_data() {
  $data = array();

  $data['node']['addtocart'] = array(
    'title' => t('Add to cart link'),
    'help' => t('Add "Add to cart" form'),
    'field' => array(
      'handler' => 'excart_views_handler_field_addtocart',
    ),
    'join' => array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
      ),
    )
  );

  return $data;
}
