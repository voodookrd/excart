<?php

/*
 * if you can replace price or other data of cart item use
 * hook_excart_cart_update()
 */
#Implements of hook_excart_cart_update()

function hook_excart_cart_update(&$cart) {
  #set sale 5% if product qty great 3
  foreach ($cart['items'] as $nid => &$item) {
    if ($item['qty'] > 3)
      $item['price'] -= round($item['price'] * 0.05, 2);
  }
}

/*
 * You can alter item data on Add Process
 */
#Implements of hook_excart_object_prepare_alter

function hook_excart_object_prepare_alter(&$item) {
  #change items title - add suffix " - SALE"
  $item['title'] .= ' - SALE';
}

#You can alter item id
#Implements of hook_excart_item_id_alter()

function hook_excart_item_id_alter(&$id, $item) {
  if ($item['price'] > 1000) {
    $id = $id . '_good';
  }
}

#You can alter data before theming in cart page
#implements hook_excart_cart_view_alter()

function hook_excart_cart_view_alter(&$data) {
  foreach($data['products'] as &$product){
    #add Happy new year to product titles in cart
    $proudct['title'] .= ' - Happy new year';
  }
}
