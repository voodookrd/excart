<?php

function excart_cart_page() {
  $cart = excart_get_cart();
  $total = excart_calculate_cart_total($cart, TRUE);
  $items = $cart['items'];
  if (empty($items)) {
    return t('Cart is empty');
  }
  foreach ($items as $id => &$item) {
    $attr_plus['attributes']['class'] = array('up', 'use-ajax');
    $attr_minus['attributes']['class'] = array('down', 'use-ajax');
    $attr_del['attributes']['class'] = array('delete', 'use-ajax');
    $plus = l(t('+'), 'cart/ajax/' . $id . '/add/nojs', $attr_plus);
    $minus = l(t('-'), 'cart/ajax/' . $id . '/rem/nojs', $attr_minus);
    $del = l(t('Delete'), 'cart/ajax/' . $id . '/del/nojs', $attr_del);
    $item['options'] = array(
      'row_attributes_array' => array(
        'class' => array("p-" . $id)
      ),
      'node_url' => url('node/' . $id),
    );
    $item['links'] = array(
      'add' => $plus,
      'remove' => $minus,
      'delete' => $del
    );
    $item['qty_html'] = '<input id="current-qty-' . $id . '" class="form-text" type="text" size="5" name="qty" value="' . $item['qty'] . '">';
    $price = $item['price'];

    $item['price_html'] = excart_format_price($price, $id);
    $item['total_price'] = excart_format_price($item['price'] * $item['qty'], $id . "t");
  }

  $count = excart_get_total_count();
  $dec = is_int($total) ? 0 : 2;
  $total = $total;
  $data = format_plural($count, '1 product, !total rub', '@count products, !total rub', array('!total' => $total));
  $total_text = $data;
  $checkout_link = l(t('Clean cart'), 'cart/clear');
  $clear_link = l(t('Clean cart'), 'cart/clear');
  $ex_path = drupal_get_path('module', 'excart');
  $output_array = array(
    'products' => $items,
    'total' => $data,
    'info' => array(
      'count' => $count,
      'total' => $total
    ),
    'checkout' => $checkout_link,
    'clear' => $clear_link,
    'files' => array(
      'js' => array(
        $ex_path . '/excart.js'
      ),
      'css' => array(
        $ex_path . '/excart.css'
      )
    )
  );
  drupal_alter('excart_cart_view', $output_array);
  $output = theme('excart_cart', $output_array);
  drupal_add_library('system', 'drupal.ajax');
  return $output;
}

function excart_add_to_cart($id) {
  #@todo add cart without js page
  return 'add to cart page';
}

function excart_ajax_callback($id, $mode) {

  if ($mode == 'nojs') {
    drupal_set_message(t('Plesae switch on JavaScript'));
    drupal_goto('cart/add/' . $id);
  }
  $count = 1;
  if (!empty($_GET['qty'])) {
    $count = $_GET['qty'];
  }

  excart_cart_add_item($id, $count);
  $total = excart_get_total_price();
  $count = excart_get_total_count();

  if (isset($_GET['hasjs'])) {
    stmmod_get_cart_block(TRUE);
    exit;
  }
  else {

    $cart_block = excart_get_cart_block();
    $commands[] = ajax_command_replace('#excart-cart-block', render($cart_block));
    if (isset($_GET['overlay'])) {
      $commands[] = ajax_command_invoke('.cart-products-preview', 'show');
      $commands[] = ajax_command_invoke('.show-cart-adv ', 'addClass', array('visible'));
    }
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}

function excart_ajax_edit($id, $action, $mode) {
  if ($mode == 'nojs') {
    drupal_set_message(t('Plesae switch on JavaScript'));
    drupal_goto('cart/change/' . $id . '/' . $action);
  }
  excart_change_cart_items($id, $action);

  $cart = excart_get_cart();
  $total = excart_get_total_price();
  $count = excart_get_total_count();
  $total_html = excart_format_price($total);
  $commands[] = ajax_command_html('.cart .info', '<div>' . $total_html . '</div>');
  $commands[] = ajax_command_replace('.total-data .price', $total_html);
  $cart_block = excart_get_cart_block();
  $commands[] = ajax_command_replace('#excart-cart-block', render($cart_block));
  if (isset($_GET['overlay'])) {
    $commands[] = ajax_command_invoke('.cart-products-preview', 'show');
    $commands[] = ajax_command_invoke('.show-cart-adv ', 'addClass', array('visible'));
  }
  if (!empty($cart['items'][$id])) {
    $qty = $cart['items'][$id]['qty'];
    $price = $cart['items'][$id]['price'];
    $price_html = excart_format_price($qty * $price, $id . 't', FALSE);

    $commands[] = ajax_command_replace('#ex-' . $id . 't', $price_html);
    $commands[] = ajax_command_invoke('#current-qty-' . $id, 'val', array($qty));
  }
  elseif (empty($cart['items'])) {
    $commands[] = ajax_command_html('#block-system-main .content #cart-page', t('Cart is empty'));
  }
  else {
    $commands[] = ajax_command_remove('.p-' . $id);
  }
  $alter_args = array(
    'id' => $id,
    'action' => $action
  );
  $total_price_html = excart_format_price($cart['total']);
  $commands[] = ajax_command_html('#total-price', $total_price_html);
  drupal_alter('excart_cart_change', $commands, $alter_args);

  return array('#type' => 'ajax', '#commands' => $commands);
}

function excart_cart_clear() {
  excart_cart_clear_cart();
  drupal_goto('cart');
}

function excart_set_page($id, $count) {
  $cart = excart_get_cart();
  if ($count == 'rem') {
    $res = excart_change_cart_items($id, 'rem');
    if ($res) {
      $cart = $res;
    }
  }
  else {
    if (!empty($cart['items'][$id])) {
      if ($count) {
        $cart['items'][$id]['qty'] = $count;
      }
      else {
        unset($cart['items'][$id]['count']);
      }
    }
  }

  $total = excart_calculate_cart_total($cart, TRUE);
  $dec = is_int($sum) ? 0 : 2;
  $data['total'] = number_format($total, $dec, '.', ' ');
  $sum = $count * $cart['items'][$id]['price'];
  $dec = is_int($sum) ? 0 : 2;
  $data['sum'] = number_format($sum, $dec, '.', ' ');
  $count = 0;
  if (!empty($cart['items'])) {
    foreach ($cart['items'] as $item) {
      $count+=$item['qty'];
    }
  }
  $data['pcount'] = $count;
  drupal_json_output($data);
  exit;
}

