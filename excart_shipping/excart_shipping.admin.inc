<?php
function excart_shipping_admin_settings(){
  $out = array();
  $out['shipping_price_settings'] = array(
      'title' => array(
          '#markup' => t('Set shipping prices settigns'),
          '#prefix' => '<h2>',
          '#suffix' => '</h2>'
      ),
      '#prefix' => '<div class="container shipping-price">',
      '#suffix' => '</div>',
      'form' => drupal_get_form('excart_shipping_price_settings_form'),

  );
  return $out;
}
function excart_shipping_price_settings_form($form, $form_state) {
  $form = array();
  $settings = excart_shipping_get_settings();
  $form['group'] = array(
      '#title' => t('Select group'),
      '#type' => 'select',
      '#options' => excart_shipping_get_groups(),
      '#default_value' => isset($settings['group']) ? $settings['group'] : NULL,
      '#required' => TRUE,
      '#ajax' => array(
          'wrapper' => 'group-field-wrapper',
          'callback' => 'excart_shipping_settings_ajax_callback'
      )
  );
  $form['field_group'] = array(
      '#prefix' => '<div id="group-field-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'container'
  );

  if (isset($form_state['values']['group']) || isset($settings['group'])) {
    if(isset($form_state['values']['group'])){
      $group = $form_state['values']['group'];
    }else{
      $group = $settings['group'];
    }
    $form['field_group']['field'] = array(
        '#title' => t('Select field for shipping prices'),
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => excart_shipping_get_groups_fields($group),
        '#default_value' => isset($settings['field'])?$settings['field']:NULL,
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
  );

  return $form;
}

function excart_shipping_settings_ajax_callback(&$form, &$form_sate) {
  return $form['field_group'];
}

function excart_shipping_admin_settings_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  excart_shipping_set_settings($form_state['values']);
  drupal_set_message(t('Shipping settings has ben saved'));
}
