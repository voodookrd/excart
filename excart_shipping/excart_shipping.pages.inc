<?php

function excart_shipping_delivery_form($form, $form_state) {

  $form = array();
  $items_count = excart_get_total_count();
  if (!$items_count) {
    drupal_goto('cart');
  }
  $shipping_types = array(
      1 => t('Shipping'),
      2 => t('Pickup'),
  );
  $form['#attributes']['class'][] = 'shipping-form';
  $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Select shipping type'),
      '#options' => $shipping_types,
      '#title_display' => 'invisible',
      '#default_value' => 1,
      '#suffix' => '<span class="or seperator">' . t('Or') . '</span>'
  );
  $form['shipping'] = array(
      '#type' => 'container',
      '#prefix' => '<div class="type-shipping type-wrapper">',
      '#suffix' => '</div>',
      '#tree' => FALSE,
      '#states' => array(
          'visible' => array(
              ':input[name="type"]' => array('value' => 1),
          )
      )
  );
  $list = excart_shipping_get_list();
  foreach ($list as $id => $li) {
    $delivery_options[$id] = $li->name;
  }
  $form['shipping']['destination'] = array(
      '#type' => 'select',
      '#title' => t('Select district'),
      '#empty_option' => t('Slelect district'),
      '#options' => $delivery_options,
      '#description' => t('For more details view !delivery', array(
          '!delivery' => l(t('delivery conditions'), '#', array('external' => true))
      )),
      '#ajax' => array(
          'callback' => 'excart_shipping_get_price_by_district_ajax',
      )
  );
  $price = '<span class="price-raw">0</span><span class="rub">i</span></strong>';
  $info_text = t('Your distinct delivery price:');
  $form['shipping']['right_info'] = array(
      '#prefix' => '<div class="right-block">',
      '#suffix' => '</div>',
      '#type' => 'container'
  );
  $form['shipping']['right_info']['delivery'] = array(
      '#type' => 'markup',
      '#markup' => '<strong>' . $info_text . '</strong><div class="price-value" id="price-value">' . $price . '</div>',
      '#prefix' => '<div class="price-value-wrap">',
      '#suffix' => '</div>'
  );
  $total_text = t('Order total price');
  $total = excart_get_total_price();

  $total_price = t('with delivery price:');
  $form['shipping']['right_info']['total'] = array(
      '#type' => 'markup',
      '#markup' => $total_text . '<br>' . $total_price . '<div class="total-price-value" id="total-price-value">' . excart_format_price($total, FALSE, FALSE, FALSE) . '</div> <span>руб.</span>',
      '#prefix' => '<div class="price-value-wrap">',
      '#suffix' => '</div>'
  );
  $form['pickup'] = array(
      '#type' => 'container',
      '#prefix' => '<div class="type-pickup type-wrapper">',
      '#suffix' => '</div>',
      '#tree' => FALSE,
      '#states' => array(
          'visible' => array(
              ':input[name="type"]' => array('value' => 2),
          )
      )
  );
  $pickup_text = voodoo_helper_get_field_by_varname('pickup');
  $form['pickup']['text'] = array(
      '#type' => 'markup',
      '#markup' => $pickup_text,
      '#prefix' => '<p>',
      '#suffix' => '</p>'
  );
  $form['contacts'] = excart_get_shipping_contact_form();
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Select payment method')
  );
  return $form;
}

function excart_get_shipping_contact_form() {
  $form = array(
      '#prefix' => '<div class="contacts-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'container',
      '#tree' => FALSE
  );
  $form['text'] = array(
      '#markup' => t('Fileds, selected by %symbol has required', array('%symbol' => '*')),
      '#prefix' => '<div class="form-description">',
      '#suffix' => '</div>'
  );
  if (user_is_anonymous()) {
    $register_link = l(t('log in'), 'user/login');
    $form['anonym_info'] = array(
        '#type' => 'markup',
        '#markup' => t('If you alredy registred !register for speedly complating order', array('!register' => $register_link)),
        '#prefix' => '<div class="green-wrapper status-block">',
        '#suffix' => '</div>'
    );
  }
  $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#attributes' => array(
          'placeholder' => t('Your name')
      )
  );
  $form['contact'] = array(
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE
  );
  $form['contact']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#attributes' => array(
          'placeholder' => t('E-mail')
      ),
      '#element_validate' => array('excart_get_shipping_email_validate'),
  );
  $form['contact']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone number'),
      '#required' => TRUE,
      '#title_display' => 'invisible',
      '#attributes' => array(
          'placeholder' => t('Phone number')
      )
  );

  $form['delivery_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Delivery address'),
      '#required' => TRUE,
      '#title_display' => 'invisible',
      '#attributes' => array(
          'placeholder' => t('Delivery address')
      ),
      '#states' => array(
          'visible' => array(
              ':input[name="type"]' => array('value' => 1),
          )
      )
  );
  $form['comment'] = array(
      '#title' => t('Comment to delivery'),
      '#type' => 'textarea',
      '#title_display' => 'invisible',
      '#attributes' => array(
          'placeholder' => t('Comment to delivery, section, floor, homephoone'),
      ),
      '#resizable' => FALSE
  );
  return $form;
}
