<?php

function excart_settings_page($form, $form_state) {
  if (user_access('ExCart root access')) {
    $form['excart_node_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node type selection'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      );

    $form['excart_node_settings']['excart_node_type'] = array(
      '#type' => 'select',
      '#title' => t('Select node type'),
      '#default_value' => variable_get('excart_node_type', 0),
      '#options' => array(0 => t('--none--')) + node_type_get_names(),
      '#ajax' => array(
        'wrapper' => 'excart-type-wrapper',
        'callback' => 'excart_ajax_select'
        )
      );
    if (isset($form['excart_node_settings']['excart_price_field']['#options'])) {
      $stype = $form['excart_node_settings']['excart_price_field']['#options'];
    } else
    $stype = FALSE;
    $form['excart_node_settings']['excart_price_field'] = array(
      '#type' => 'select',
      '#title' => t('Select price field'),
      '#default_value' => variable_get('excart_price_field', 0),
      '#options' => $stype ? $stype : excart_get_price_fields(),
      '#prefix' => '<div id="excart-type-wrapper">',
      '#suffix' => '</div>',
      '#validated' => TRUE
      );
  }
  $form['excart_default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
    );
  $form['excart_default']['excart_delivery'] = array(
    '#title' => t('Default delivery price'),
    '#type' => 'textfield',
    '#default_value' => variable_get('excart_delivery', 150),
    );

  $form['excart_price_format'] = array(
    '#type' => 'fieldset',
    '#title' => t('Price formating'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
    );
  $form['excart_price_format']['excart_curr'] = array(
    '#title' => t('Currency format'),
    '#type' => 'textfield',
    '#default_value' => variable_get('excart_curr', 'RUB')
    );
  $form['excart_price_format']['excart_show_price'] = array(
    '#title' => t('Show label'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('excart_show_price', 0)
    );

  $form['#submit'][] = 'excart_save_feidls';
  return system_settings_form($form);
}

function excart_ajax_select(&$form, &$form_state) {
  $type = $form_state['values']['excart_node_type'];
  $form['excart_node_settings']['excart_price_field']['#options'] = excart_get_price_fields($type);
  return $form['excart_node_settings']['excart_price_field'];
}

function excart_get_price_fields($type = FALSE) {
  $price_field = array();
  if ($type === FALSE)
    $type = variable_get('excart_node_type', FALSE);
  if ($type) {
    $fields = field_info_instances('node', $type);
    foreach ($fields as $name => $field) {
      $price_field[$name] = $field['label'];
    }
  }
  return $price_field;
}

function excart_save_feidls(&$form, &$form_state){
  if(!empty($form_state['values']['excart_node_type'])){
    $type = $form_state['values']['excart_node_type'];
    foreach(array('excart_add_to_cart') as $field_name){
      $field = field_info_field($field_name);
      if (empty($field)) {
        $field = array(
          'field_name' => $field_name,
          'type' => 'excart_add_to_cart',
          'entity_types' => array('node'),
          );
        field_create_field($field);
      }
      $instance = field_info_instance('node', $field_name, $type);
      if (empty($instance)) {
        $instance = array(
          'field_name' => $field_name,
          'label' => t('Add to cart form'),
          'description' => 'Attach Add to cart form to product',
          'entity_type' => 'node',
          'bundle' => $type,
          );
        field_create_instance($instance);
      }
    }
  }
}