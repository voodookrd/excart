<?php

/*
 * обработка данных перед отправкой в файл темизации
 *
 */
#implements Hook_process_excart_cart()

function excart_process_excart_cart(&$vars) {
  foreach ($vars['products'] as &$product) {
    $product['options']['row_attributes'] = drupal_attributes($product['options']['row_attributes_array']);
  }
  if (!empty($vars['files']['js'])) {
    $js = $vars['files']['js'];
    if (!is_array($js)) {
      $js = array($js);
    }
    foreach ($js as $file_name) {

      drupal_add_js($file_name);
    }
  }
  if (!empty($vars['files']['css'])) {
    $css = $vars['files']['css'];
    if (!is_array($css)) {
      $css = array($css);
    }
    foreach ($css as $file_name) {
      drupal_add_css($file_name);
    }
  }
}

# if you can change the price template, copy above function into your
# template.php file, change the returned data by your conditions
# and rename it THEME_NAME_excart_format_price

function theme_excar_format_price($vars) {
  $price = $vars['price'];
  $id = $vars['id'] ? $vars['id'] : '';
  $wrapper = $vars['wrapper'];
  $ps = $vars['ps'];
  $iprice = (int) $price;
  if ($price == $iprice) {
    $dec = 2;
  } else {
    $dec = 0;
  }
  $price = number_format($price, $dec, '.', ' ');
  $show = variable_get('excart_show_price', 0);

  if ($wrapper) {
    $out = array(
        '#prefix' => '<span class="price">',
        '#suffix' => '</span>'
    );
  }

  if($ps){
  $out['price'] = array(
      '#markup' => $price,
      '#prefix' => ($show ? t('Price') : '') . ' <strong' . ($id ? (' id="ex-' . $id . '"') : '') . '>',
      '#suffix' => '.-</strong>'
  );
  }else{
    $out['price']['#markup'] = $price;
  }
  return render($out);
}

function excart_process_excart_cart_block(&$vars) {
  $vars['cart_count'] = format_plural($vars['count'], '1 product, !total rub', '@count products, !total rub', array('!total' => $vars['cart']['total']));
  $vars['count_plural'] = format_plural($vars['count'], '1 product', '@count products');
  $vars['total_html'] = excart_format_price($vars['cart']['total']);
  foreach ($vars['cart']['items'] as &$item) {
    if(empty($item['price_html'])){
      $item['price_html'] = excart_format_price($item['price']);
    }
  }
  if (!empty($vars['files']['js'])) {
    $js = $vars['files']['js'];
    if (!is_array($js)) {
      $js = array($js);
    }
    foreach ($js as $file_name) {

      drupal_add_js($file_name);
    }
  }
  if (!empty($vars['files']['css'])) {
    $css = $vars['files']['css'];
    if (!is_array($css)) {
      $css = array($css);
    }
    foreach ($css as $file_name) {
      drupal_add_css($file_name);
    }
  }
}
