(function($) {
  Drupal.behaviors.ExCart = {
    attach: function(context, settings) {
      $('.remove link-wrapper').on('click', '.rem-btn', function(e) {
        e.preventDefault();
        var url = $this.attr('href');
        $.ajax({
          url: url,
          type: 'GET',
          success: function(data) {
            location.reload();
          }
        });
      });
    }
  }
})(jQuery);