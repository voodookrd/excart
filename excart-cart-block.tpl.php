<div class="cart">
  <?php if ($show['price']) : ?>
    <div class="total"><?php print $cart['total']; ?></div>
  <?php endif; ?>
  <?php if ($show['qty']) : ?>
    <div class="count"><?php print $count; ?></div>
  <?php endif; ?>
  <div class="links">
    <a href="/cart">Goto cart</a>
  </div>
</div>